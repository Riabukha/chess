import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int height, width;
        do {
            System.out.print("Укажите высоту: ");
            height = numberReader(scanner);

            System.out.print("Укажите ширину: ");
            width = numberReader(scanner);

        } while (height <= 0 || width <= 0);
        Chess chess = new Chess(height, width);
        chess.build();

    }

    private static int numberReader(Scanner scanner) {
        int selectedNumber = 0;
        boolean isError = true;

        do {
            String numberString = scanner.nextLine();
            try {
                selectedNumber = Integer.parseInt(numberString);
                if (selectedNumber < 0) {
                    System.out.print("Число не должно быть отрицательным, повторите: ");
                } else if (selectedNumber == 0) {
                    System.out.print("Число не должно быть равно нулю, повторите: ");
                } else {
                    isError = false;
                }
            } catch (Exception ex) {
                System.out.print("Ошибка, введите обычное число без знаков: ");
            }
        } while (isError);
        return selectedNumber;
    }
}
